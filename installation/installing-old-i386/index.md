---
title: Installing old i386 images
description:
icon:
weight:
author: ["gamb1t",]
---

At the time of writing this page, September 18, 2024, i386/x86 kernels are still being built, but this is going to change soon. We don't know exactly when i386 will no longer be built, but it will [likely be in Linux 6.11](https://lists.debian.org/debian-release/2024/09/msg00220.html). If this is accurate, there will likely be no more i386 images starting with the 2024.4 release.

While the number of users who need i386 images is low, those who do will will want a way to get an iso that they can use. To do this, they can use [old.kali.org](https://old.kali.org/kali-images/kali-2024.2/) and download the last iso where i386 was built.